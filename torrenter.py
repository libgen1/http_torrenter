"""Library Genesis Re-Seeder and Leecher by ScorchedEarth

Usage:
  libgen.py search <term>...
  libgen.py get <id> [--http]
  libgen.py gettorrentfromhttp <torrent>
  libgen.py (-h | --help)
  libgen.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --http        Use HTTP first before trying torrent swarm

Info:

A library genesis script.

Commands:

search              Search libgen. Enter your search terms. Searches author + title.
get                 Get a book from libgen. Pass it the book ID from search.
gettorrentfromhttp  Pass a torrent ID (i.e. 2439000) and it will populate the correct dir with http versions for re-seeding.

Setup info:

1. create a directory for library genesis
2. put a 'torrents' folder inside that folder and download the libgen torrents into it
3. adjust the 'libgen_dir' variable below to point to the right place
4. [optional] download the latest libgen database dump and import it into mysql
5. fill out the database details below - set the user to '' if you don't want to use a local db (you can only then use gettorrentfromhttp to re-seed)
6. install the requirements (docopt, mysql-connector) and the command line tools aria2 and wget
"""

from docopt import docopt

import subprocess
import sys

import mysql.connector
import shutil
import io
import sys
import os
from collections import OrderedDict

mirror = 'http://93.174.95.29/main/{0}/{1}/'
libgen_dir = ''
db_host = 'localhost'
db_user = ''
db_password = ''
db_database = ''

class librarygen:

    mydb = None

    def __init__(self):

        if db_user != '':
            self.mydb = mysql.connector.connect(
                host=db_host,
                user=db_user,
                passwd=db_password,
                database=db_database)

    def search(self, searchterms):
        mycursor = self.mydb.cursor()

        search = ""

        for term in searchterms:
            if search == '':
                search += "SELECT * FROM updated WHERE (Title Like '%{0}%'".format(term)
            else:
                search += " AND Title Like '%{0}%'".format(term)

        search += ')'

        first = True

        for term in searchterms:
            if first:
                search += " OR (Author Like '%{0}%'".format(term)
                first = False
            else:
                search += " AND AUTHOR Like '%{0}%'".format(term)

        search += ')'

        mycursor.execute(search)

        myresult = mycursor.fetchall()

        for x in myresult:
            result = '{0}: {1}, {2} [{3}]'.format(x[0], x[5], x[1], x[36])
            print(result)

    def parse_torrent(self, torrent_file):
        toz = extract_torrent_metadata('{1}torrents/r_{0}.torrent'.format(torrent_file, libgen_dir))

        #mycursor = self.mydb.cursor()
        bucket = int(torrent_file)

        for file in toz.file_name_list:
            search = "SELECT * FROM updated WHERE MD5 = '{0}'".format(file)

            #mycursor.execute(search)

            #myresult = mycursor.fetchone()

            hash = file.upper()
            hash_lower = hash.lower()

            url = mirror.format(bucket, hash_lower)

            try:
                os.mkdir('{1}{0}/'.format(bucket, libgen_dir))
            except:
                pass
            cmd = 'wget -O {3}{1}/{2} {0}'.format(url, bucket, hash_lower, libgen_dir)
            print(cmd)
            subprocess.run(cmd.split(' '), stdout=sys.stdout)
            for subdir, dirs, files in os.walk(libgen_dir):
                for file in files:
                    if file == hash:
                        print('SUCCESS')



    def get(self, id, http_first=False):
        # get a hash
        mycursor = self.mydb.cursor()

        search = "SELECT * FROM updated WHERE ID = {0}".format(id)

        mycursor.execute(search)

        myresult = mycursor.fetchone()

        hash = myresult[37]
        hash_lower = hash.lower()

        bucket = int((int(id) / 1000)) * 1000

        url = mirror.format(bucket, hash_lower)

        torrent_file = None

        new_file = '{0} - {1}, {2}.{3}'.format(myresult[0], myresult[5], myresult[1], myresult[36])

        for subdir, dirs, files in os.walk(libgen_dir):
            for file in files:
                if file == hash or file == hash_lower:
                    shutil.copyfile(os.path.join(subdir, file), new_file)
                    print('Already downloaded. Copied to {0}.'.format(new_file))
                    return

        if http_first:
            print("File not found on disk. Checking HTTP first.")
            try:
                os.mkdir('{1}{0}/'.format(bucket, libgen_dir))
            except:
                pass
            cmd = 'wget -O {3}{1}/{2} {0}'.format(url, bucket, hash_lower, libgen_dir)
            print(cmd)
            subprocess.run(cmd.split(' '), stdout=sys.stdout)
            for subdir, dirs, files in os.walk(libgen_dir):
                for file in files:
                    if file == hash or file == hash_lower:
                        shutil.copyfile(os.path.join(subdir, file), new_file)
                        print('Already downloaded. Copied to {0}.'.format(new_file))
                        return

        # see if the file is in a torrent - this needs moving lower
        print("File not found on disk. Checking torrents.")
        for r, d, files in os.walk('{0}torrents/'.format(libgen_dir)):
            for file in files:
                if '.torrent' in file:

                    toz = extract_torrent_metadata(os.path.join('{0}torrents/'.format(libgen_dir), file))
                    if has_file(toz, hash):
                        print("Found file in {0}".format(os.path.join('{0}torrents/'.format(libgen_dir), file)))
                        toz = extract_torrent_metadata(os.path.join('{0}torrents/'.format(libgen_dir), file))
                        torrent_file = os.path.join('{0}torrents/'.format(libgen_dir), file)
                        id = toz.file_name_list.index(hash) + 1
                        break

        if torrent_file:
            print('Downloading file {0} from torrent'.format(id))
            cmd = 'aria2c {1} --select-file={0} --bt-remove-unselected-file --bt-exclude-tracker=* --bt-tracker=udp://tracker.openbittorrent.com:80 --seed-time=0 --dir={2}'.format(id, torrent_file, libgen_dir)
            print(cmd)
            subprocess.run(cmd.split(' '), stdout=sys.stdout)
        else:
            print('Not found in torrents on disk.')
            return

        # see if the file is on disk
        for subdir, dirs, files in os.walk(libgen_dir):
            for file in files:
                if file == hash or file == hash_lower:
                    shutil.copyfile(os.path.join(subdir, file), new_file)
                    print('Already downloaded. Copied to {0}.'.format(new_file))
                    return


# --- Program options (from command line)
__prog_options_override_torrent_dir = 0
__prog_options_deleteWrongSizeFiles = 0
__prog_options_truncateWrongSizeFiles = 0
__prog_options_deleteUnneeded = 0


# Unified torrent information object. Works for torrent files with 1 or several
# files.
class Torrent:
    torrent_file = None
    dir_name = None
    piece_length = 0
    num_pieces = 0
    num_files = 0
    file_name_list = []
    file_length_list = []
    pieces_hash_list = []
    pieces_file_list = []


# --- Get size of terminal ---
# shutil.get_terminal_size() only available in Python 3.3
# https://docs.python.org/3/library/shutil.html#querying-the-size-of-the-output-terminal
# print(sys.version_info)
if sys.version_info < (3, 3, 0):
    # Disable long text-line chopping
    __cols = -1
    print('[NOTE] Your Python version is lower than 3.3.0. Terminal size cannot be determined.')
    print('[NOTE] Chopping of long text lines disabled.')
else:
    __cols, __lines = shutil.get_terminal_size()
    # print('{0} cols and {1} lines'.format(__cols, __lines))


# --- Bdecoder ----------------------------------------------------------------
class DecodingError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class Decoder:
    def __init__(self, data: bytes):
        self.data = data
        self.idx = 0

    def __read(self, i: int) -> bytes:
        """Returns a set number (i) of bytes from self.data."""
        b = self.data[self.idx: self.idx + i]
        self.idx += i
        if len(b) != i:
            raise DecodingError(
                "Incorrect byte length returned between indexes of {0} and {1}. Possible unexpected End of File."
                    .format(str(self.idx), str(self.idx - i)))
        return b

    def __read_to(self, terminator: bytes) -> bytes:
        """Returns bytes from self.data starting at index (self.idx) until terminator character."""
        try:
            # noinspection PyTypeChecker
            i = self.data.index(terminator, self.idx)
            b = self.data[self.idx:i]
            self.idx = i + 1
            return b
        except ValueError:
            raise DecodingError(
                'Unable to locate terminator character "{0}" after index {1}.'.format(str(terminator), str(self.idx)))

    def __parse(self) -> object:
        """Selects the appropriate method to decode next bencode element and returns the result."""
        char = self.data[self.idx: self.idx + 1]
        if char in [b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', b'0']:
            str_len = int(self.__read_to(b':'))
            return self.__read(str_len)
        elif char == b'i':
            self.idx += 1
            return int(self.__read_to(b'e'))
        elif char == b'd':
            return self.__parse_dict()
        elif char == b'l':
            return self.__parse_list()
        elif char == b'':
            raise DecodingError('Unexpected End of File at index position of {0}.'.format(str(self.idx)))
        else:
            raise DecodingError('Invalid token character ({0}) at position {1}.'.format(str(char), str(self.idx)))

    def decode(self):
        """Start of decode process. Returns final results."""
        if self.data[0:1] not in (b'd', b'l'):
            return self.__wrap_with_tuple()
        return self.__parse()

    def __wrap_with_tuple(self) -> tuple:
        """Returns a tuple of all nested bencode elements."""
        l = list()
        length = len(self.data)
        while self.idx < length:
            l.append(self.__parse())
        return tuple(l)

    def __parse_dict(self) -> OrderedDict:
        """Returns an Ordered Dictionary of nested bencode elements."""
        self.idx += 1
        d = OrderedDict()
        key_name = None
        while self.data[self.idx: self.idx + 1] != b'e':
            if key_name is None:
                key_name = self.__parse()
            else:
                d[key_name] = self.__parse()
                key_name = None
        self.idx += 1
        return d

    def __parse_list(self) -> list:
        """Returns an list of nested bencode elements."""
        self.idx += 1
        l = []
        while self.data[self.idx: self.idx + 1] != b'e':
            l.append(self.__parse())
        self.idx += 1
        return l


# If max_length == -1 it means size of terminal could not be determined. Do
# nothing witht the string.
def limit_string_lentgh(string, max_length):
    if max_length > 1 and len(string) > max_length:
        string = (string[:max_length - 1] + '*');

    return string


# Convert a list of bytes into a path
def join_file_byte_list(file_list_bytes):
    file_list_string = []
    for i in range(len(file_list_bytes)):
        file_list_string.append(file_list_bytes[i].decode("utf-8"))

    return '/'.join(file_list_string)


# Returns a Torrent object with torrent metadata
__debug_torrent_extract_metadata = 0


def extract_torrent_metadata(filename):
    torrentFileName = filename
    torrent = Torrent()
    torrent.file_name_list = []
    torrent.torrent_file = filename

    torrent_file = open(torrentFileName, "rb")
    # Use internal Bdecoder class
    decoder = Decoder(torrent_file.read())
    torr_ordered_dict = decoder.decode()
    info_ordered_dict = torr_ordered_dict[b'info']

    if __debug_torrent_extract_metadata:
        print('=== Dumping torrent root ===')
        for key in torr_ordered_dict:
            print(' key {0} value {1}'.format(key, torr_ordered_dict[key]))

        print('=== Dumping torrent info ===')
        for key in info_ordered_dict:
            print(' key {0} value {1}'.format(key, info_ordered_dict[key]))

    # If torrent info has files field then torrent has several files
    if b'files' in info_ordered_dict:
        t_name = info_ordered_dict[b'name']  # Directory name to store torrent
        t_piece_length = info_ordered_dict[b'piece length']
        t_files_list = info_ordered_dict[b'files']

        # --- Converts the string into a file-like object
        t_pieces = info_ordered_dict[b'pieces']
        pieces = io.BytesIO(t_pieces)
        # --- Ensure num_pieces is integer
        num_pieces = len(t_pieces) / 20
        if not num_pieces.is_integer():
            print('num_pieces {0} is not integer!'.format(num_pieces))
            sys.exit(1)
        num_pieces = int(num_pieces)

        # --- Fill torrent object
        torrent.dir_name = t_name.decode("utf-8")
        torrent.piece_length = t_piece_length
        torrent.num_pieces = num_pieces
        torrent.num_files = len(t_files_list)
        for i in range(num_pieces):
            hash = pieces.read(20)
            torrent.pieces_hash_list.append(hash)
        torrent.total_bytes = 0
        for t_file in t_files_list:
            torrent.file_name_list.append(join_file_byte_list(t_file[b'path']))
            # print(type(t_file[b'length'])) # type is <class 'int'>
            torrent.file_length_list.append(t_file[b'length'])
            torrent.total_bytes += t_file[b'length']

        # DEBUG
        if __debug_torrent_extract_metadata:
            print(' Directory {0}'.format(t_name))
            print(' Piece length {0}'.format(t_piece_length))
            print(' Number of pieces {0}'.format(num_pieces))
            print(' Number of files {0}'.format(len(t_files_list)))
            print(' len(t_pieces) =  {0}'.format(len(t_pieces)))
            print(' num_pieces * piece_length = {0}'.format(num_pieces * t_piece_length))
            print(' len(torrent.pieces_hash_list) = {0}'.format(len(torrent.pieces_hash_list)))

    # Single file torrent
    else:
        t_name = info_ordered_dict[b'name']  # File name11
        t_piece_length = info_ordered_dict[b'piece length']
        t_length = info_ordered_dict[b'length']

        # --- Converts the string into a file-like object
        t_pieces = info_ordered_dict[b'pieces']
        pieces = io.BytesIO(t_pieces)
        # --- Ensure num_pieces is integer
        num_pieces = len(t_pieces) / 20
        if not num_pieces.is_integer():
            print('num_pieces {0} is not integer!'.format(num_pieces))
            sys.exit(1)
        num_pieces = int(num_pieces)

        # --- Fill torrent object
        torrent.piece_length = t_piece_length
        torrent.num_pieces = num_pieces
        torrent.num_files = 1
        torrent.file_name_list.append(t_name.decode("UTF-8"))
        torrent.file_length_list.append(t_length)
        for i in range(num_pieces):
            hash = pieces.read(20)
            torrent.pieces_hash_list.append(hash)
        torrent.total_bytes = t_length

        # DEBUG
        if __debug_torrent_extract_metadata:
            print(' Filename {0}'.format(t_name))
            print(' Size {0}'.format(t_length))
            print(' Piece length {0}'.format(t_piece_length))
            print(' Number of pieces {0}'.format(num_pieces))
            print(' Number of files {0}'.format(1))
            print(' len(t_pieces) =  {0}'.format(len(t_pieces)))
            print(' num_pieces * piece_length = {0}'.format(num_pieces * t_piece_length))
            print(' len(torrent.pieces_hash_list) = {0}'.format(len(torrent.pieces_hash_list)))

    # Make a list of files for each piece. Should include also the file offsets.
    # This is to find torrent that has padded files that must be trimmend.
    # Many Linux torrent clients have this bug in ext4 filesystems.
    # [ [{'file_idx': 0, 'start_offset': 1234, 'end_offset': 5678},
    #    { ... } ],
    #   [  ...   ],
    #   ...
    # ]
    piece_length = torrent.piece_length
    pieces_file_list = []
    piece_current_length = 0
    this_piece_files_list = []
    for i in range(torrent.num_files):
        file_dict = {}
        file_dict['file_idx'] = i
        file_dict['start_offset'] = 0
        file_size = file_current_size = torrent.file_length_list[i]
        while True:
            remaining_piece_bytes = piece_length - piece_current_length
            if file_current_size > remaining_piece_bytes:
                piece_current_length += remaining_piece_bytes
                file_current_size -= remaining_piece_bytes
            else:
                piece_current_length += file_current_size
                file_current_size = 0
            # Go for next file if no more bytes
            if file_current_size == 0:
                file_dict['end_offset'] = file_size
                this_piece_files_list.append(file_dict)
                break
            # Piece is ready, add to the list
            file_dict['end_offset'] = file_size - file_current_size
            this_piece_files_list.append(file_dict)
            pieces_file_list.append(this_piece_files_list)
            # Reset piece files list and size
            piece_current_length = 0
            this_piece_files_list = []
            # Add current file to piece files list
            file_dict = {}
            file_dict['file_idx'] = i
            file_dict['start_offset'] = file_size - file_current_size
    # Last piece
    if piece_current_length > 0:
        pieces_file_list.append(this_piece_files_list)

    # Put in torrent object
    torrent.pieces_file_list = pieces_file_list

    # DEBUG: print list of files per piece
    if __debug_torrent_extract_metadata:
        for piece_idx in range(len(pieces_file_list)):
            print('Piece {0:06d}'.format(piece_idx))
            this_piece_files_list = pieces_file_list[piece_idx]
            for file_idx in range(len(this_piece_files_list)):
                file_dict = this_piece_files_list[file_idx]
                print(' File {0:06d} start {1:8d} end {2:8d}'
                      .format(file_dict['file_idx'], file_dict['start_offset'], file_dict['end_offset']))

    return torrent


def has_file(torrent, file):
    # --- Print list of files
    if file in torrent.file_name_list or file.lower() in torrent.file_name_list:
        return True
    else:
        return False


def list_torrent_contents(torrent):
    print('Printing torrent file contents...')

    # --- Print list of files
    text_size = 7 + 17 + 1
    print('    F#            Bytes  File name')
    print('------ ----------------  --------------')
    for i in range(len(torrent.file_name_list)):
        print('{0:6} {1:16,}  {2}'.format(i + 1, torrent.file_length_list[i], \
                                          limit_string_lentgh(torrent.file_name_list[i], __cols - text_size)))

    # --- Print torrent metadata
    print('')
    print('Torrent file      : {0}'.format(torrent.torrent_file))
    print('Pieces info       : {0:10,} pieces, {1:16,} bytes/piece'
          .format(torrent.num_pieces, torrent.piece_length))
    print('Files info        : {0:10,} files,  {1:16,} total bytes'
          .format(torrent.num_files, torrent.total_bytes))
    if torrent.num_files > 1:
        print('Torrent directory : {0}'.format(torrent.dir_name))

    return 0

# Checks single file against SHA1 hash for integrity
__debug_file_location_in_torrent = 0


def main(args):
    if 'search' in args and args['search']:
        searcher = librarygen()
        searcher.search(args['<term>'])
    elif 'gettorrentfromhttp' in args and args['gettorrentfromhttp']:
        searcher = librarygen()
        searcher.parse_torrent(args['<torrent>'])
    elif 'get' in args and args['get']:
        searcher = librarygen()
        if '--http' in args and args['--http']:
            searcher.get(args['<id>'], True)
        else:
            searcher.get(args['<id>'], False)

if __name__ == "__main__":
    arguments = docopt(__doc__, version='0.1')
    main(arguments)