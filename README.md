# HTTP torrenter

### Author: ScorchedEarth

```
Usage:
  libgen.py search <term>...
  libgen.py get <id> [--http]
  libgen.py gettorrentfromhttp <torrent>
  libgen.py (-h | --help)
  libgen.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --http        Use HTTP first before trying torrent swarm

Info:

A library genesis script.

Commands:

search              Search libgen. Enter your search terms. Searches author + title.
get                 Get a book from libgen. Pass it the book ID from search.
gettorrentfromhttp  Pass a torrent ID (i.e. 2439000) and it will populate the correct dir with http versions for re-seeding.

Setup info:

1. create a directory for library genesis
2. put a 'torrents' folder inside that folder and download the libgen torrents into it
3. adjust the 'libgen_dir' variable below to point to the right place
4. [optional] download the latest libgen database dump and import it into mysql
5. fill out the database details in torrenter.py - set the user to '' if you don't want to use a local db (you can only then use gettorrentfromhttp to re-seed)
6. install the requirements (docopt, mysql-connector) and the command line tools aria2 and wget
```

